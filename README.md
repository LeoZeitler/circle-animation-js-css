# CIRCLE SNIPPET #
The circle snippet is easy to handle. The trick is setting the **padding** value same like the **width** value in CSS. Take a look on the files and run the example. Additionally a fade in effect is implemented in jQuery when the page is loaded. You do not need to install anything since jQuery is binded via CDN.
